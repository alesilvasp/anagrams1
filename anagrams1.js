const button = document.getElementById("findButton");
const container = document.getElementById("container")
const anagramsDiv = document.createElement('div');
anagramsDiv.classList.add('container__anagrams');

button.addEventListener("click", function () {
    let typedText = document.getElementById("input").value;
    let anagramsSpan = document.createElement('span');
    anagramsDiv.innerHTML = '';
    anagramsSpan.innerText = getAnagramsOf(typedText);
    anagramsDiv.appendChild(anagramsSpan)
    container.appendChild(anagramsDiv);

    // seu código vai aqui
});

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function getAnagramsOf(word) {
    let digitedWord = alphabetize(word);
    let arrAnagrams = [word];

    for (let i = 0; i < palavras.length; i++) {
        let wordPalavras = alphabetize(palavras[i]);

        if (digitedWord === wordPalavras) {
            if (!arrAnagrams.includes(palavras[i]))
                arrAnagrams.push(palavras[i])
        }
    }
    if (arrAnagrams.length > 1) {
        return arrAnagrams.join(', ');
    }
    return "Que pena! Não encontramos nenhum Anagrama"
}